#!/usr/bin/env php
<?php

echo "Starting Server\n";
$pid = trim(shell_exec('nohup /usr/bin/php -S 0:8000 >/dev/null 2>&1 & echo $!'));

echo "Sleeping 1 second to ensure server starts\n";
sleep(1);

$test_count = 10000;
echo "Using $test_count tests each\n";

$test = function ($hostname) use ($test_count) {
    $tests = [];

    foreach (range(1, $test_count) as $i) {
        $time = microtime(true);
        file_get_contents("http://${hostname}:8000/static.txt");
        $tests[] = (microtime(true) - $time);
    }

    $time = number_format((array_sum($tests) / count($tests)) * 1000, 3);
    $max = number_format(max($tests) * 1000, 3);
    $min = number_format(min($tests) * 1000, 3);

    echo "${hostname} [avg: ${time}ms] [min: ${min}] [max: ${max}]\n";
};

$test("127.0.0.1");
$test("localhost");
$test("127.0.0.1.nip.io");


echo "Ending Server [$pid]\n";
posix_kill($pid, SIGINT);
